﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChaosArray
{
    class ChaosException : Exception
    {
        public ChaosException(string message): base(message) {}
    }
}
