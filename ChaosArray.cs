﻿using System;
using System.Collections.Generic;

namespace ChaosArray
{
    public class ChaosArray<T>
    {
        private T[] internalArray;
        private Random rng = new Random();

        // Because an array of type int will fill itself wil "0" and not "null" I use a dictionary to keep track of changes since array init
        private Dictionary<int, bool> indexModified = new Dictionary<int, bool>();
        
        public int MaxSize { get; set; }


        public ChaosArray(int maxSize)
        {
            MaxSize = maxSize;
            internalArray = new T[maxSize];

            InitIndexModified(maxSize);
        }


        /// <summary>
        /// Insert into a random index
        /// </summary>
        public void Insert(T item)
        {
            int insertAt = rng.Next(0, MaxSize);
            if (indexModified[insertAt])
                throw new ChaosException("There already exists an item at index " + insertAt);

            indexModified[insertAt] = true;
            internalArray[insertAt] = item;

            Console.WriteLine("Inserted into index " + insertAt);
        }
        
        /// <summary>
        /// Get a random entry from the array
        /// </summary>
        public T Retrieve()
        {
            int retrieveAt = rng.Next(0, MaxSize);

            if (!indexModified[retrieveAt])
                throw new ChaosException("There is nothing at index " + retrieveAt);

            return internalArray[retrieveAt];
        }

        /// <summary>
        /// Reset the array
        /// </summary>
        public void Clear()
        {
            internalArray = new T[MaxSize];
            InitIndexModified(MaxSize);
        }

        /// <summary>
        /// Fill the IndexModified control dictionary with false for each index
        /// </summary>
        private void InitIndexModified(int size)
        {
            for (int i = 0; i < size; i++)
            {
                indexModified[i] = false;
            }
        }

    }
}
