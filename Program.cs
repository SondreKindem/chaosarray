﻿using System;

namespace ChaosArray
{
    class Program
    {
        static void Main(string[] args)
        {
            ChaosArray<int> array = new ChaosArray<int>(5);

            for (int i = 0; i < 6; i++)
            {
                try
                {
                    array.Insert(i);
                }
                catch(ChaosException ex)
                {
                    Console.WriteLine($"Uh oh! {ex.Message}");
                }
            }

            for (int i = 0; i < 10; i++)
            {
                try
                {
                    Console.WriteLine(array.Retrieve());
                }
                catch(ChaosException ex)
                {
                    Console.WriteLine($"Uh oh! {ex.Message}");
                }
            }
        }
    }
}
